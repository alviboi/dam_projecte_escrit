---
title: Control d’assistència i entrevistes a centres
author: [Alfredo Rafael Vicente Boix]
date: "07-03-2022"
parallaxBackgroundImage: ./fons.png
parallaxBackgroundSize: 6000px 1080px
---

# Punt de partida

---

### Situació del CEFIRE

1. Sense WIFI gestionada
2. Sistema control de fitxatges
3. Entrevistes a centres 
4. Gestió d'assistència a formacions
5. Imatge corporativa

# Planificació del projecte

---

### Solucions plantejades

1. Adaptar xarxa WIFI
2. Aplicació mòbil amb encriptació
3. Aplicació per a gestió d'assistència
4. Aplicació Assessories
   1. Mòdul de control de visites
   2. Mòdul de gestió d'assistència a cursos
   3. Generació d'informes fitxatges
   4. Generació documents corporatius

---

### Diagrama de Gantt

![Diagrama de Gantt](imatges/img/gantt.jpg)

# Adaptar estructura

---

### Esquema actual

![Esquema de la xarxa del CEFIRE](imatges/img/cefire_diagrama.png){ style="background-color: white; height: 600px;" } 

---

### Unifi Controller

![Unifi Controller](imatges/img/controller_unifi.jpg)

---

### Base de dades

<div style="-webkit-column-count: 2; -moz-column-count: 2; column-count: 2; -webkit-column-rule: 1px line #e0e0e0; -moz-column-rule: 1px line #e0e0e0; column-rule: 1px dotted #e0e0e0;">
<div style="display: inline-block;">
![Disseny de la base de dades original](imatges/img/2bd.jpg){ style="background-color: white; height: 400px; padding: 10px;" }
</div>
<div style="display: inline-block;">
![Disseny de la base de dades de l'aplicació](imatges/img/3bd.jpg){ style="background-color: white; height: 400px; padding: 10px;" }
</div>
</div>

# Aplicació mòbil

---

### Funcionament i requisits

![Infraestructura](imatges/appmob/inf.png){ style="height: 500px;" } 

---

### Aplicació Android

<div style="-webkit-column-count: 3; -moz-column-count: 3; column-count: 3; -webkit-column-rule: 1px line #e0e0e0; -moz-column-rule: 1px line #e0e0e0; column-rule: 1px dotted #e0e0e0;">
<div style="display: inline-block;">
![Panell de control](imatges/appmob/2.png){ style="background-color: white; height: 400px; padding: 10px;" }
</div>
<div style="display: inline-block;">
![Sincronització](imatges/appmob/3.png){ style="background-color: white; height: 400px; padding: 10px;" }
</div>
<div style="display: inline-block;">
![Escollir centre visita](imatges/appmob/4.png){ style="background-color: white; height: 400px; padding: 10px;" }
</div>
</div>

---

### Aplicació Android

<div style="-webkit-column-count: 3; -moz-column-count: 3; column-count: 3; -webkit-column-rule: 1px line #e0e0e0; -moz-column-rule: 1px line #e0e0e0; column-rule: 1px dotted #e0e0e0;">
<div style="display: inline-block;">
![Creant visita](imatges/appmob/5.png){ style="background-color: white; height: 400px; padding: 10px;" }
</div>
<div style="display: inline-block;">
![Llistat de centres](imatges/appmob/6.png){ style="background-color: white; height: 400px; padding: 10px;" }
</div>
<div style="display: inline-block;">
![Llistat de visites](imatges/appmob/7.png){ style="background-color: white; height: 400px; padding: 10px;" }
</div>
</div>

# Aplicació de l'Assessoria

---

### Entrada i accés principal

<div style="-webkit-column-count: 2; -moz-column-count: 2; column-count: 2; -webkit-column-rule: 1px line #e0e0e0; -moz-column-rule: 1px line #e0e0e0; column-rule: 1px dotted #e0e0e0;">
<div style="display: inline-block; margin-top: 40px;">
![Login](imatges/appass/1.png){ style="background-color: white; padding: 10px;" }
</div>
<div style="display: inline-block;">
![Control principal](imatges/appass/2.png){ style="background-color: white; padding: 10px;" }
</div>


# Mòdul visites a centres

---

### Llistat visites

![](imatges/appvisites/2.png)

---

### Editar visita

![](imatges/appvisites/3.png){ style="height: 500px;" }

---

### Informe de visita

![](imatges/appvisites/5.png){ style="height: 500px;" }

---

### Exemple d'ús

<!--VIDEO-->
![]()


# Control d'assistència

---

### Abordar dos aplicacions

* Mòdul de control d'assistència
* Aplicació de l'Aula

---

### Estudi de OpenCV i estratègies d'ús

![](imatges/appaula/groc.png)

---

### Pantalla d'espera

![](imatges/control_ass/1.png)

---

### Selecció de curs

![](imatges/control_ass/2.png)

---

### Identificació

![](imatges/control_ass/4.png)

---

### Mòdul de control d'assistència - Gesform

![Gesform](imatges/appaula/1.jpg)

---

### Pantalla de control

![](imatges/appaula/2.png)

---

### Arrastrem i soltem

![](imatges/appaula/3.png)

---

### Emplenem dades

![](imatges/appaula/4e.png)

---

### Afegim dispositius

![](imatges/appaula/5.png)

---

### Llistat de dispositius

![](imatges/appaula/6.png)

---

### Llistat de cursos

![](imatges/appaula/7.png)

---

### Enviar orde d'arranc

![](imatges/appaula/8e.png)

---

### Descarrega acta d'assistència

![](imatges/appaula/10e.png)

---

### Informe d'assistència

![](imatges/appaula/12.png)

---

### Problemes trobats

![](imatges/appaula/13.png)

# Exporta informes

---

### Escollix data

![Pantalla control](imatges/exp_inf/1.png)

---

### Exporta informe

![Exporta](imatges/exp_inf/2.png)

---

### Informe

![Informe generat](imatges/exp_inf/3.png)

# Maquetació

---

### Identitat digital

![Generant docx](imatges/maquetacio/4.png)

---

### Arrastrem i soltem

![](imatges/maquetacio/6.png)

---

### Traducció a markdown

![](imatges/maquetacio/7.png)

---

### Propietats del document

![Propietats](imatges/maquetacio/8.png)

---

### Exportem pdf 

![Generant pdf](imatges/maquetacio/9.png)

---

### Disseny de pdf

![](imatges/maquetacio/10.png)

---

### Exportem zip

![Generant zip](imatges/maquetacio/12.png)

---

### Importem a Aules

![Aules](imatges/maquetacio/14.png)

---

### Aules

![Finestra aules](imatges/maquetacio/15.png){ height=500px }

# Conclusions

---

### Conclusions

* Facilitat d'ús personal amb pocs coneixement tècnics
* Sobredimensionat per al temps
* Utilització de ferramentes alternatives a les clàssiques
* Projecció professional
* Futures actualitzacions
* Desplegament i proves
* Manteniment llibreries encriptació i altres

# Moltes gràcies per la vostra tasca!!!

