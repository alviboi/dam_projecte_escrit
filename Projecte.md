---
title: "Sistema de gestió de control d'assistència i entrevistes a centres"
author: [Alfredo Rafael Vicente Boix]
date: "03-06-2022"
subject: "fitxar"
keywords: [fitxar, Instal·lació]
subtitle: "Aplicació mòbil de visites, aplicació de control d'assistència, generador d'informes i maquetació de documents"
lang: "ca"
page-background: "background10.PDF"
titlepage: true,
titlepage-rule-color: "360049"
titlepage-background: "background10.PDF"
colorlinks: true
toc-own-page: true
header-includes:
- |
  ```{=latex}
  \usepackage{awesomebox}
  ```
pandoc-latex-environment:
  noteblock: [note]
  tipblock: [tip]
  warningblock: [warning]
  cautionblock: [caution]
  importantblock: [important]
...

<!-- \awesomebox[violet]{2pt}{\faRocket}{violet}{Lorem ipsum…} -->


\vspace*{\fill}

![](img/cc.png){ height=50px }

Aquest treball així com el codi que apareix en ell està subjecte a una llicència creative commons que permet la seua difusió ús comercial reconeguent sempre l'autoria del seu creador. Aquest document es troba per a poder ser modificat al següent repositori de github:

<!-- CANVIAR L'ENLLAÇ -->
[https://github.com/alviboi/script_base_datos/tree/main/Projecte](https://gitlab.com/alviboi/dam_projecte_escrit)
\newpage

**Resumen del projecte**

Una de les prioritats del CEFIRE de VAlència és el estar constantment informat i tenir un coneixement actiu del que està passant als centres, així com de la evolució que aquests experimenten al llarg del cursos. La eina principal que tenen les assessories per adquirir aquest coneixement és l'entrevista personal amb l'assessor i l'equip directiu. Però cada assessor té la seua metodologia pròpia i algunes de les entrevistes queden documentades en papers o senzillament en la ment del propi assessor que té el coneixement del centre però es perd quan se'n va del CEFIRE. És per això que es planteja la creació d'una eina que estiga en tot moment disponible i puga centralitzar les dades obtingudes en un base de dades per a que tothom al CEFIRE puga consultar-les.

En tot moment s'ha de respectar la normativa vigent en quant a les dades i aquestes no han de quedar exposades en cap moment a Internet. 

Per altra banda, arran de la pandèmia del COVID, també es planteja la necessitat de perfeccionar el sistema de control d'assistència a les sessions presencials. En aquest punt es veu necessari que:

* El sistema siga fiable i no done lloc a confusió.
* Que hi haja el mínim contacte possible dels assistents a superficies. O dit d'altra manera, que no intercanvien bolígraf i full de signatures.

És per això que es planteja un sistema de control d'assistència que permeta d'una manera factible i senzilla portar el registre dels assistents i fer-ho el més transparent possible per a l'assessor. Donada la negativa de la DGTIC a donar accés a la base de dades d'Ítaca, s'ha de crear un sistema de gestió autònom, sense requerir en cap moment de l'accés a Ítaca.

Paral·lelament, al CEFIRE es disposa d'un sistema de control de fitxatges per al assessors, aquest sistema està molt ben implementat i compleix la seua tasca a la perfecció. Des de la Subdirecció General del Professorat s'ha demanat que es genere un informe amb l'assistència mensual del assessors. Per tant es planteja també la creació d'una aplicació que permeta la crear els informes d'assistència dels assessors amb un click.

Altra de les problemàtiques plantejades és el tema de la imatge corporativa al CEFIRE. Actualment no existeix un model d'imatge corporativa, a més, el sistema que es planteje, ha de permetre mantenir la imatge corporativa tan als documents creats per a *Aules* com els PDF generats.

\newpage

**Abstract**

One of the priorities of the CEFIRE of VALÈNCIA is to be constantly informed and have an active of what is going on at the centres, as well as his evolution that these experiment along the courses. The main tool of the advisors to acquire this acknowledge is the personal interview with the advisor. But each advisor has his own methodology and some of the interviews stay documented at papers or simply at the mind of the advisor that has the ken of the centre, but misses when he leaves the CEFIRE. That is why it is important the creation of a tool available for all the crew. This tool can centralize the data at one database, thus everybody at the CEFIRE will be able to consult it.

At all times it has to respect the law regarding the data and these do not have to stay exposed at any moment on the Internet. 

On the other hand, as a result of the COVID's strike, also poses the goal to perfect the system of assistance at the sessions. At this point it is necessary that:

* The system will be reliable and easy to use.
* There was the minimal contact of the assistants to the sames objects. Or to put in other words, do not exchange pen and sheet of signatures.

That is teh reason wht a system of control of assitance that allow the registry of the participants, and make it the most transparent possible for the advisor. Given the refusal of the DGTIC at giving access to the database of Itaca, it has to be created a system self-managed, without requiring at any moment of the access to Itaca.

Simultaneously, at the CEFIRE has of one system of control of assitance for the advisors, this system is very well implemented and very realiable. From the Subdirecció General of the Professorat has been requested that it generates a report with the monthly assistance of the advisors. Therefore it is important   
create of a tool that allow the creation of the report of the advisors with one click.

Other of the problematic exposed is the issue of the corporative image at the CEFIRE. At present it does not exist a model of corporate image, besides, the system that we should plan, has to permit maintain the corporate image for the documents created for *Aules* as well the PDF generated.

\newpage

# Introducció

Actualment el CEFIRE de València no diposa de cap aplicació per a mantindre actualitzades les dades de les entrevistes que es realitzen als centres, i tota eixa informació no s'ha mantingut i s'ha perdut al llarg dels diferents cursos perdent així la perspectiva que dona el coneixement sobre l'evolució del centre. Es planteja alguna solució que permeta a l'assessor mantenir actualitzades les dades de les entreviste i que aquesta informació siga accessible per tot el personal del CEFIRE.

Així mateix, cal ingeniar algun sistema que permeta el control d'assistència al cursos de cara al proper curs en el qual es minimitze el contacte entre els assistents i permeta tenir centralitzat les dades d'assistència. S'ha de permetre poder imprimir un informe d'assistència de cadascuna de les sessions per a poder ser adjuntat l'arxiu definitiu del curs. El sistema ha de ser bastant flexible com per a permetre a un sol assessor gestionar diversos cursos al mateix temps.

Per altra banda des de la subdirecció arriben instruccions que encomanen als directors dels CEFIRE a portar un control de fitxatges dels assessors. Afortunadament aquest sistema ja està correctament implementat al CEFIRE de València. Caldria disposar d'un sistema per a generar els informes de fitxatges de cadascun dels assessors per a poder ser enviat a la Subdirecció. De manera que interrompira el mínim possible la tasca diària del CEFIRE.

## Problemàtiques existents

Al CEFIRE al igual que tots els centres de la Comunitat Valenciana existeixen dues xarxes, la xarxa de gestió anomenada xarxa de secretaria i la xarxa per a l'alumnat anomenada xarxa d'Aules. La xarxa de secretaria està securitzada i gestionada per la DGTIC i teòricament és segura. És per això que s'insta al centres a que facen un bon ús de la xarxa i a no connectar cap punt d'accés a esta xarxa, per a evitar possibles filtracions.

Això presenta una problemàtica afegida ja que els dispositius que s'utilitzen per a fer les entrevistes de les visites, s'han de connectar per cable o busca alguna alternativa per a connectar la base de dades del CEFIRE al dispositiu de la manera més segura possible.

## Possibles solucions

Actualment existeixen poques solucions que donen resposta a unes problemàtiques tan concretes com aquestes. Per tant caldrà que es realitze una solució personalitzada del problema. En l'apartat de crear una plantilla corporativa per al CEFIRE existeixen dos solucions lliures viables, com són ExeLearning, programari dissenyat per l'INTEF que permet generar documents per Aules, encara que els PDF generats no tenen un format atractiu. 

A més a més, ExeLearning és un programa que es presenta complexe per a poder ser utilitzat de manera ràpida i senzilla per les assessories, per tant queden descartades totes els altres opcions comercials que existeixen.

Es planteja la possibilitat d'utilitzar plantilles de LaTeX per a donar-li un caire més professional i la eina *pandoc* per a formatar els materials. Caldrà centrar-se en estudiar una manera de facilitar la tasca al assessor. **La plantilla en sí i el disseny no és unaprioritat sinó la simplificació del procediment.**

## Proposta de realització

Actualment el CEFIRE de València allotja el seus servidor en un entorn **Proxmox** de la següent manera:

![Diagrama del CEFIRE](imatges/img/cefire_diagrama.png){ height=280px }

Per tant la proposta s'ha d'adaptar a l'entorn actualment existent. Els servidor actualment en funcionament són:

* Servidor NFS
* Servidor de l'aula d'informàtica 1.
* Servidor de l'aula d'informàtica 2.
* Servidor web de la plataforma de fitxatges.
* Servidor d'altres serveis per a secretaria.
* Servidor DSpace

Es proposa dissenyar una aplicació de manera modular que done solució als quatre problemes plantejats. A més aquesta modularitat permetrà futures ampliacions per altres aplicacions. Així, d'aquesta manera tindrem quatre aplicacions modulars de la següent manera-

* Aplicació per a gestionar les visites del assessors
* Aplicació per a controlar l'assistència.
* Aplicació per a exportar els informes de fitxatges.
* Aplicació de maquetació (sense les plantilles)

Després de valorar la realització de les plantilles es deixarà a l'assessoria artística el dissenys de les mateixes i només s'implementarà la viabilitat tècnica de la mateixa.

Després de valorar les diferents opcions s'ha fet un sondeig entre els diferents assessors i la majoria tenen un mòbil amb Android, a excepció de la direcció que no duu centres, per tant es planteja la possibilitat de disposar d'una aplicació mòbil per a fer les visites, amb la problemàtica d'escriure eixes dades en la base de dades dins de la base de dades a la xarxa de secretaria.

Per al control d'assistència, es valorar la possibilitat de poder mostrar el DNI a una webcam a l'aula on es va a realitzat el curs i que aquest agafe les dades del DNI i registre l'assistència.

Per tant a banda de les quatre aplicacions plantejades caldrà implementar:

* Aplicació mòbil per a Android per a la visita de centres, de manera que l'assessor sempre tindrà al seua abast l'aplicació per a fer la visita. 
* Aplicació que estarà a l'aula on realitze la formació, aquesta estarà cablejada amb la xarxa de secretaria.
* Controlador per a l'aplicació mòbil que es muntarà a una màquina virtual que es connectarà a través d'una connexió virtual dins de **Proxmox** amb un firewall que només deixarà obert el port 3306 per a la base de dades MYSQL.
* Caldrà muntar una wifi al CEFIRE amb els dispositius del quals disposa el CEFIRE de València de la manera més eficient possible i que puga gestionar de manera generalitzada.

## Solució tècnica, possibles solucions a implementar

Caldrà escollir quines tecnologies es van a utilitzar en el desenvolupament del projecte. Dins de les diferents opcions cal tenir en compte diversos aspectes.

* En primer lloc cal considerar que els ordinadors de les assessories porten el Sistema Operatiu de la Comunitat Valenciana LLiureX.
* Ha de presentar facilitat en el seu ús i implantació.
* Caldria considerar que en un futur existeix la possibilitat que els ordinadors de les assessories presentaren Windows, per tant s'ha de desenvolupar en un llenguatge àmpliament contrastat amb l´us d'ambdues plataformes.
* A la DGTIC, tot i que no hauria de de ser un punt a considerar és important tenir en compte que a la Conselleria d'Educació s'ha utilitzat Java per a programar les diferents aplicacions. I s'està plantejant opcionalment emprar **JavaFx** en les seues aplicacions. Tot i que no es tracta d'un informació oficial, per corporativisme i projecció professional es planteja també la possibilitat d'emprar aquesta tecnologia.

Les possibles solucions són:

- Primerament, caldrà afegir una nova màquina virtual al **Proxmox**, donat que el CEFIRE té dispositius **Unifi** AC-LR, que estan en EOL (*End-of-life cycle*). Però ja que no es disposa de pressupost s'utilitzaran aquest dispositius on:
  - S'actualitzarà el firmware dels mateixos
  - Es muntarà un controlador (el propi de **Unifi**) per a poder gestionar tota la xarxa. Caldrà instal·lar un servidor wifi addicional,
- Android Studio o Quasar per a desenvolupar l'aplicació a Android. Quasar presenta l'avantatge de poder crear aplicacions amb IOS. Però finalment es decidix per Android Studio ja que es tracta d'un eina més utilitzada i pot emprar Java, el que estandaritza el projecte.
- Python amb **Glade** o Java amb **JavaFx**. L'opció sens dubte que més avantatges presenta és Python amb **Glade**, ja que és molt senzilla d'utiltizar, Python presenta un entorn on pots afegir llibreries amb extrema facilitat i és molt versàtil. Finalment, tot i els nombrosos pros, es decidix emprar **JavaFx** amb Java per motius acadèmics i ampliar coneixement dins d'altres entorns. Cal afegir que pot facilitar la projecció professional (cal tenir en compte que, en principi, no vaig a eixir de la empresa en la que treballe).
- Per al reconeixement d'imatges s'han plantejat altres opcions tals com:
  - MS Computer Vision
  - SimpleCV
  - OpenCV
- Finalment s'opta per emprar la llibreria Opencv ja que és la que major simplicitat presenta a l'hora de ser utilitzada, encara que hi han moltes opcions dispars. La llibreria de Microsoft queda descartada completament.
- Per la creació d'informes es planteja la possibilitat d'utilitzar JasperReports ja que és una eina àmpliament utilitzada per a la generació d'informes. Finalment es decidix utilitzar PDFBox ja que està àmpliament contrastada i els informes no presenten major complexitat comper a utilitzat JasperReports.
- Per a la maquetació dels documents s'emplearan finalment plantilles LaTeX i CSS.

# Planificació i desenvolupament del projecte

## Requisits de seguretat

Un dels aspectes que cal tenir en compte és la seguretat de comunicació que s'ha d'establir a l'hora de sincronitzar la informació de les visites amb la base de dades del CEFIRE. En aquest punt tenim diferents aproximacions que cal tenir en compte:

* L'aplicació de fitxatges del CEFIRE utilitza la llibreria bcrypt per a encriptar les contrassenyes, per tant la identificació del usuari i la seua contrasenya caldrà que utilitzen la mateixa llibreria. 
* El controlador per a la connexió en l'aplicació mòbil utilitza *Javax.crypto* el qual no presenta major problemàtica que dotar ambdós programes de una clau per a poder decodificar-se la informació que s'envien. De manera que s'utilitza el wifi però sempre amb les dades encriptades.
* La llibreria jbcrypt de Java presenta un problema. Com podem observar hem de llevar-li els tes primers números del hash rebuts i canviar-los per **$2a** ja que la llibreria php de laravel utilitza la versió **$2b**, però pareix ser que són compatibles. De l'altra manera no es podia fer la comprovació de la contrasenya.

```Java
public Boolean comprova_usuari() {
		user = userdao.get_User(usuari);
		String password_hash = "$2a" +  user.getPassword().substring(3);
		boolean result1 = BCrypt.checkpw(contrassenya, password_hash);
		return result1;
	}
  ```
:::note
A la llibreria **BCrypt.class** es pot comprovar que en tot moment les comprovacions es fan sobre la cadena $2a, i en cap moment sobre la cadena $2b. 
:::

## Planificació amb projectlibre

Es plantejarà només l'aparat de desenvolupament del projecte ja que no es diposa de temps suficient per a poder desplegar-lo i passar fase de proves. S'han plantejat les següents accions dins del projecte. Cal tenir en compte que no s'inclou la part de creació de la màquina virtual **Unifi** ja que ha rebut suport institucional i s'utilitzaran hores de feina per al seu muntatge:

**Posada a punt de la xarxa del CEFIRE i MV**

- Instal·lació i configuració dels switchs i cablejat.
- Instal·lació i configuració de **Proxmox**
- Adequació dels ordinadors de l’Assessoria
- Configuració i instal·lació de Servidor Ubuntu - **Unifi** Controller.
- Configuració i instal·lació de Servidor de Serveis.

**Aplicació mòbil**

- Crear controlador
- Crear aplicació importació centres
- Crear visites
- Editar visites
- Sincronització de visites
- Proves i correcció d'errors

**Aplicació fitxatges Aula**

- Anàlisi de les diferents opcions
- Investigació dels perifèrics, funcionament, llibreries i documentar-se al respecte.
- Disseny del prototip
- Programació control de webcam
- Classe base de dades
- Entorn de usuari
- Solució d’errades i testeig

**Aplicació control de fitxatges e importació**

- Anàlisis de la estructura
- Importació Excel
- Exportació a aplicació fitxatges
- Control remot
- Gestió de dispositius
- Testeig i errades

**Exportació d’informes de fitxatges**

- Valoració de llibreries PDFBOX i Jasperreports
- Disseny exportació PDF
- Creació d'entorn i aplicació
-Testeig i errades

**Creació de maquetació de documents**

- Disseny entorn i editor
- Creació de plantilles
- Creació de scripts i estudi de **pandoc**
- Testeig i errades

:::tip
Al projectlibre no li hem configurat el treball en cap de setmana, però aquest considera que es treballen 8h al dia, aquest fet ha servit per a compensar la diferència d'hores ja al mateix temps que s'estava desenvolupant el projecte s'ha estat treballant activament.
::: 

![Diagrama de Gantt](imatges/img/gantt.jpg){ height=240px }

:::note
En tot moment per a fer el seguiment del codi s'ha fet ús del github en diferents repositoris. Aquests repositoris estan oberts i es pot comprovar el codi en tot moment.
:::

# Posada a punt de la xarxa del CEFIRE i MV

Es procedix a la creació de la màquina virtual que farà de controlador per a la sincronització de les visites i es farà la instal·lació del controlador **Unifi**. AL CEFIRE es diposes ja d'uns dispositius wifi emmagatzemats. El primer que cal fer eś actualitzar el firmware a l'últim que està disponible, com que **Unifi** ja no dona suport als dispositius la actualització ha de fer-se manualment.

En aquesta imatge el detalla el procediment d'actualització del firmware. I es passa a l'adopció de tots el dispositius **Unifi**.

![Actualització dels punts **Unifi**](imatges/img/unifi_terminal.jpg){ height=270px }

Aquí tenim la màquina virtual funcionant amb l'**Unifi** Controller:

![**Unifi** controller](imatges/img/controller_unifi.jpg){ height=270px }

# Base de dades

Actualment es diposa d'una base de dades amb MYSQL funcionant amb docker al servidor del CEFIRE. S'ha optat per l'opció d'afegir algunes taules per a centralitzar la base de dades i per a poder mostrar en un futur les dades dins de l'aplicació de fitxatges del CEFIRE.

![Disseny actual de la base de dades](imatges/img/1bd2.png){ height=240px }

A aquest disseny s'han afegit les següents taules:

![Taules afegides](imatges/img/2bd.jpg){ height=180px }

![Taules afegides](imatges/img/2bd.jpg){ height=180px }

:::caution
El primer que crida l'atenció és el fet de tenir dades redundants, però s'ha posat en una mateixa taula el dni i les sessions de participació per a facilitar la connexió de l'aplicació de l'aula per a controlar l'assistència
:::


# Aplicació mòbil

En aquest cas hem de parlar del binomi controlador-aplicació ja que un no s'entén sense l'altre.

## Aplicació mòbil

L'aplicació mòbil ha de tenir la capacitat de poder enviar les dades encriptades ja que s'enviaran a través de la wifi del CEFIRE, es per això que s'implementa la llibreria *Javax.crypto* tan al controlador com a l'aplicació mòbil.

![Aplicació mòbil](imatges/appmob/1.png){ height=220px }

L'aplicació disposa de tres tasques fonamentals:

* L'entrevista de la visita als centres, amb tres pantalles amb les qüestions fonamental i un apartat d'observacions per si es volen afegir més coses.
* El llistat de centres que l'assessor té assignat. Aquests centres es guarden a la base de dades de l'aplicació (**SQLITE**) connectant-se directament al controlador per a descarregar les dades (l'intercanvi es fa amb un array JSON).
* Finalment tenim l'apartat sincronització que s'encarrega de fer les sincronitzacions, això sí cal estar connectat a la xarxa del CEFIRE per a poder fer la connexió de manera viable.

![Panell de control](imatges/appmob/2.png){ height=220px }

Dins de l'apartat sincronitzacions es posa les dades del controlador que es facilitaran al assessor i el seu usuari i contrasenya. A més es diposa d'un botó per a reinicialitzar les bases de dades per a quan es canvie de curs.

![Sincronització](imatges/appmob/3.png){ height=220px }

Quan es fa la entrevista es pot escollir a quin centre es fa la entrevista i són tres pantalles on en la tres es poden guardar les dades

![Visita](imatges/appmob/4.png){ height=220px }


![Guarda visita](imatges/appmob/5.png){ height=200px }

Hi han centres que no s'utilitzen perquè no estan fent formació eixe any, aquests es poden esborrar per a tenir-ho tot més organitzat.

![Esborrar centres](imatges/appmob/6.png){ height=200px }

Així mateix es poden editar les dades d'un centre per si cal tornar a l'entrevista o esborrar-la per a millor organització una vegada ja està sincronitzada

![Edició de les enquestes](imatges/appmob/6.png){ height=200px }

\awesomebox[violet]{2pt}{\faGithub}{violet}{Es pot consultar el codi de l'aplicació al següent enllaç: https://github.com/alviboi/VisitaCentres}

## Controlador

Paral·lelament es desenvolupa un controlador que accepta les connexions dels mòbils i comprova que és un usuari del CEFIRE. Aquest controlador passarà els centres al dispositiu mòbil o sincronitzarà les visites.

\awesomebox[violet]{2pt}{\faGithub}{violet}{Es pot consultar el codi de l'aplicació al següent enllaç: https://gitlab.com/alviboi/dam\_controlador\_servidor}

# Aplicació de l'Assessoria

L'aplicació que disposarà el assessor al seu ordinador estarà plantejada de manera modular que en funció del que vullga fer anirà a un lloc o altre. Caldrà que l'assessor s'identifique amb el seu usuari i contrasenya d'assessor:

![Login](imatges/appass/1.png){ height=230px }

Una vegada dins podrà anar a cada lloc. Cada un del sistemes està programat en un paquet diferent de Java per a simplificar el seu manteniment o poder afegir altres mòduls de manera senzilla.

![Panel principal](imatges/appass/2.png){ height=220px }

:::important
Per a connectar-se a la base de dades s'ha utilitzat el framework Hibernate, per això també es disposen dels paquets domain i data.
:::

## Gestió de visites als centres

Aquesta secció de l'aplicació tindrà accés a la base de dades on s'emmagatzemen totes les entrevistes fetes als centres. En la següent imatge podem veure com apareix un llistat de totes les entrevistes.

![Llistat d'entrevistes](imatges/appvisites/1.png){ height=220px }

El sistema disposa d'un desplegable per a filtrar per assessories o per filtrar directament pel nom del centre. S'ha afegit un botó de tot a petició del client per a que elimines els filtres directament i facilitar la cerca ràpida:

![Filtre d'entrevistes](imatges/appvisites/2.png){ height=220px }

Una vegada seleccionada una entrevista es deplega una finestra on es poden modificar els camps de la mateixa per si es volen afegir algunes consideracions a l'entrevista:

![Edició d'entrevistes](imatges/appvisites/3.png){ height=220px }

:::important
Una de les coses més cridaneres d'aquest apartat és el fet que no tingues un botó per a crear una nova entrevista. Que és fàcilment implementable. Aquest característica s'ha decidit mantindre-la d'aquesta manera en consonància amb el client per a forçar que les assessories treballen amb l'aplicació mòbil i evitar el fet que utilitzen paper i després afegisquen manualment la entrevista al sistema.
En tot cas, el desplegament d'aquesta característica no portaria més d'un día-
:::

Dins de les entrevistes podem veire que existeix un botó **+** que permet expandir el camp de l'entrevista per als usuaris que s'esplaien un poc més dins de les seues entrevistes.

Una vegada fetes totes les consideracions, l'assessoria pot imprimir l'informe de l'entrevista, de manera que queda estandarditzada i servix per al seu arxiu.

![Desa entrevistes](imatges/appvisites/4.png){ height=200px }

![Informe de la visita](imatges/appvisites/5.png){ height=230px }


## Aplicació asssitència Aula

Aquest apartat pretén facilitar el control d'assistència de les diferents formacions, pel que es planteja crear un dispositiu que llegisca els dni dels assistents i faja el fitxatge de la persona assistent.

Les assessories del CEFIRE treballen en una programa de gestió anomenat **Gesform** (Gestió de formació). Aquest programa es connectat directament a la base de dades de *Itaca* a través del client instantclient de Oracle, pel que seria fàcilment implementable una connexió directa a la base de dades a través del client de Oracle. Però no ha hagut autorització expressa per a poder connectar-se a *Itaca* des d'una aplicació de creació pròpia, per tant la importació es realitzarà d'altra manera.

El **Gesform** permet l'exportació de dades d'un curs a arxiu excel (.xls), d'aquesta manera s'implementarà una funció que permetrà importar eixes dades a la nostra base de dades. 

Una de les dificultats que presenta aquesta manera de treballar, a banda d'afegir un poc més de complexitat és que l'arxiu que genera **Gesform** és molt antic. Suposadament d'una versió d'Excel anterior a l'any 97, Java no té cap llibreria (o al menys jo no l'he trobada), per a gestionar aquest tipus d'arxius de fa més de 25 anys. Podem veure una comparació de l'arxiu creat per Gesform front a un arxiu Excel de l'any 97 en aquesta imatge:

![Arxius excel](imatges/appaula/13.png){ height=200px }

Crear una llibreria nova per a importar aquest tipus d'arxius és una tasca que pot requerir molt de temps. En aquest punt la solució adoptada ,després d'estudiar diferents opcions, ha sigut convertir l'arxiu a odt amb l'ajuda d'un programa extern, en aquest cas LibreOffice i importar les dades de l'arxiu ods. Després es copia a la carpeta /tmp l'arxiu que es vol convertir. Això es faria cridant a un script extern que executa la següent orde:

```bash
/bin/sOffice --convert-to ods /tmp/convertir.xls --outdir /tmp/
```

:::important
Cal afegir, ja que ha resultat un dels punt amb el que més temps he invertit. Les instal·lacions a linux amb *snap* o *flatpak* creem un capa que aïlla l'accés als arxius del Sistema Operatiu. Per tant, es va procedir a fer una instal·lació manual de l'Eclipse per a que funcionara correctament la solució implentada.
:::

En la següent pantalla podem veure com es crea l'arxiu xls amb el **Gesform**:

![Creació d'arxiu xls](imatges/appaula/1.jpg){ height=240px }

Per a l'usuari es fa de manera totalment transparent, agafarà l'arxiu creat i bé clicant sobre el camp per a importar l'arxiu o arrastrant i soltant es podrà veure com s'han importat tots els participants:

![Importa curs](imatges/appaula/2.png){ height=230px }

![Arrastra i solta](imatges/appaula/3.png){ height=200px }

Una vegada ja estan importats tots els participant cal afegir el nom, codi i les sessions que tindrà l'acció formativa on es vol que es passe el control d'assistència:

![Completa camps](imatges/appaula/4e.png){ height=220px }

L'aplicació permet donar d'alta els dispositius que estaran a les aules per a poder començar el registre:

![Alta dispositiu](imatges/appaula/5.png){ height=200px }

I editar o esborrar els dispositius:

![Alta dispositiu](imatges/appaula/6.png){ height=200px }

També permet esborrar o comprovar els cursos:

![Llistat cursos](imatges/appaula/7.png){ height=200px }

Una vegada tenim definits tots els paràmetres d'un curs, podem afegir tants paràmetres com vullguem. Podem veure com es crea una entrada per a cadascuna de les sessions d'un curs:

![Sessions cursos](imatges/appaula/8e.png){ height=200px }

I podem escollir el dispositiu que volem arrancar:

![Dispositiu](imatges/appaula/9e.png){ height=200px }

I enviar l'orde per a que arranque el dispositiu, en el següent punt comentarem el programa que estarà ubicat a l'aula. Una vegada concloga la sessió es pot descarregar l'informe d'assistència al curs:

![Descarrega informe](imatges/appaula/10e.png){ height=200px }

![Model d'informe](imatges/appaula/12.png){ height=230px }

Les comunicacions entre els dos ordinadors es fan a través de la xarxa de secretaria i s'utilitzen sockets per a la comunicació entre el dos. La comunicació es realitza amb objectes JSON.

### Aplicació de l'aula

Per a inscriure als assistents a la sessió s'ha plantejat un aplicació que detecte el número del DNI ensenyant el document i una vegada detectat aquest quede inscrit en la base de dades. Aquest script no presenta excesva compelxitat però s'han hagut d'estudiar les diferents opcions per a poder detectar les dades dels particpants.

S'ha utilitzat la biblioteca **opencv** que permet entrenar intel·ligència artificial per a poder detectar objectes, el principal handicap que hem tingut en aquest cas és el fet de tenir només a la nostra disposició dos DNI per a poder fer les degudes comprovacions.

L'identificació dels caràcters es faria amb la biblioteca **pytesseract**.

:::note
Aquest script s'ha fet en Python ja que va ser tremendament complicat instal·lar **opencv** a *archlinux*, ja que el repositoris no funcionaven correctament i presentà moltíssims problemes a l'hora de compilar-la amb make i make install. 
:::

#### Primera aproximació

La primera aproximació, després de descartar la possibilitat d'aconseguir 100 dnis per a poder entrenar la intel·ligència artificial va ser fer-ho per colors, i extraure el requadre que coincidira amb els colors. Es va crear un xicotet script per a poder ajustar les característiques de la imatge.

![Procés d'estudi dels colors del DNI](imatges/appaula/groc.png){ height=200px }

En aquest punt, l'extracció del requadre no era precís i pytesseract no identificava bé els caràcters i donava molts errors.

:::important
Insistisc en que l'estudi de les aproximacions per a captar les dades formen part del projecte i ha sigut una feina molt tediosa i, de vegades, desesperant. No tot ha de ser escriure codi, és important estudiar l'estratègia de programació i més quan comencen a utilitzar objectes del món real
:::

#### Aproximació escollida

A banda d'aquesta aproximació comentada, es feren altres aproximacions que no resultaren fructuoses. Finalment es va decidir ja que el mateix instal·lador de **opencv** porta biblioteques entrenades per a reconeixement d'objectes utilitzar la biblioteca de reconeixement facial per a captar la imatge de la cara i estendre la captura a la mida del DNI (en proporció).

El codi del model 1 de DNI, després de tractar diverses aproximacions queda de la següent manera:

```Python

def DNI1(faces_rect, img, cropped_image):
    for (x, y, w, h) in faces_rect:

        cv2.rectangle(img, (x-round(3.1*w), y-round(1.5*h)), (x+round(1.3*w), y+round(1.4*h)), (0, 255, 0), 2)

        if ((y-round(1.5*h)>0) and  x-round(3.1*w)>0):
            cropped_image = img[y-round(1.5*h):y-round(1.5*h)+round(2.9*h), x-round(3.1*w):x-round(3.1*w)+round(4.4*w)]
            height, width, _ = cropped_image.shape     
            cropped_dni = cropped_image[round(height*0.8):height, 0:round(width*0.35)]
            text = pytesseract.image_to_string(cropped_dni)
            return Arregla_DNI(text)
```

En aquesta aproximació el que es fa després de captar la imatge del DNI sencer es retallar la part que porta el número per a comprovar a la base de dades si es pot inscriure. Existeixen dos funcions, una per a cada DNI, es podria estendre a altres documents.

La capacitat de processament és un handicap en aquesta aproximació, ja que es va tractar d'utilitzar una raspberry pi 3 per a crear la detecció i no va ser possible.

#### Funcionament

El funcionament tractarà de semblar-se al mode quiosc i es vorà la següent pantalla mentre no s'active el curs, com hem vist, es pot fer de manera remota o presencialment.

![Pantalla inici](imatges/control_ass/1.png){ height=200px }

Si cliquem en seleccionar curs ens apareixerà un llistat de tots els cursos que hem configurat amb les seues sessions. Escollim el que vullguem i ens apareixerà la pantalla per a poder realitzat la inscripció.

![Escollim el curs](imatges/control_ass/2.png){ height=200px }

Una vegada arranque es pot aproximar un DNI a la càmera per a poder detectar el número de DNI: 

![Identificació amb DNI](imatges/control_ass/4.png){ height=220px }

D'aquesta manera ja quedaria inscrit i des de l'ordinador de l'assessor ja es podria imprimir el full d'assistència.

#### Properes revisions

L'aplicació és totalment funcional, però hi han coses que s'han quedat en el tinter per falta de disponibilitat de temps. Algunes de les coses que caldria millorar en futures revisions són:

* Millorar l'aspecte estètic
* Agilitzar la capturar de dades
* Capturar les dades de tot el DNI si no està inscrita la persona

\awesomebox[violet]{2pt}{\faGithub}{violet}{Es pot consultar el codi de l'aplicació al següent enllaç: https://gitlab.com/alviboi/dam\_script\_del\_aula}

## Exportació d’informes de fitxatges

Aquesta secció crea informes de tots els assessors per a poder ser entregat a la Subdirecció general de formació del professorat. L'informe tarda un poc en generar-se ja que ha de fer-se de tots els assessors, per això té n indicador de quan està completat el informe que va creixent.

Per a poder exportar l'informe simplement es tria una data i l'aplicació buscarà en el més que s'ha escollit:

![Escollir data](imatges/exp_inf/1.png){ height=200px }

Una vegada estiga l'informe generat tenim un PDF tot junt amb tots els informes de cadascun dels assessors:

![Generació dels informes](imatges/exp_inf/2.png){ height=200px }

Els informes generats quedarien de la següent manera:

![Informe](imatges/exp_inf/3.png){ height=280px }

## Creació de maquetació de documents

Per últim tenim la maquetació de documents. Després d'estudiar les diferents opcions que hem trobat com ja hem comentat ens hem decantat per aquesta opció ha que presenta certs avantatges respecte a les altres solucions. La solució adoptada ha sigut utilitzar:

* Compilador de documents pandoc
* Adaptació de la plantilla LaTeX eisvogel
* Es planteja com a exemple un document d'estils CSS per a formatar el HTML per a ser exportat a *Aules*.

Aquesta solució entre altres coses:

* És molt més versàtil ja que es pot ajustar en tot moment perquè és independent de l'aplicació.
* Presenta una barrera d'entrada molt senzilla i a mesura que es vullguen fer coses es pot anar ampliant els coneixements al respecte. 
* Es poden utilitzar documents del pla digital i guardar-se a la carpeta de documentació de la identitat digital del centre.

Per a maquetar un document es partirà de la creació d'un document amb el word de l'Office 365:

![Creació de document](imatges/maquetacio/4.png){ height=280px }

Aquest document el guardem a l'ordinador:

![Desa del document](imatges/maquetacio/5.png){ height=200px }

Després només amb arrastrar i soltar ja tradueix el document a **markdown** i ja es pot treballar directament en ell. Si no es vol fer cap canvi només cal donar a l'icona de converteix a PDF:

![Arrastrar i soltar](imatges/maquetacio/6.png){ height=200px }

Podem veure en document ja convertir a markdown:

![Document convertit](imatges/maquetacio/7.png){ height=230px }

Cal donar-li uns paràmetres com és el títol del document o l'autor per a poder fer la conversió:

![Introdueix paràmetres](imatges/maquetacio/8.png){ height=220px }

Guardem el document i podem veure com és el disseny:

![Guardar document](imatges/maquetacio/9.png){ height=200px }

![Disseny del document](imatges/maquetacio/10.png){ height=280px }

![Disseny del document](imatges/maquetacio/11.png){ height=240px }

:::note
Aquesta plantilla esta feta coma exemple, l'assessoria de l'àmbit artístic està treballant en una plantilla **Unifi**cada.
:::

Igualment podem desar el document com un arxiu HTML que ens permetrà pujar-ho a *Aules* per a tindre el document formatat:

![Disseny del document](imatges/maquetacio/12.png){ height=200px }

Podem veure que ens crea un fitxer ZIP. Aquest fitxes el podem pujar com a "Arxiu" a qualsevol **moodle** i descomprimir-lo i posar l'arxiu index.HTML com a arxiu principal. D'aquesta manera moodle llig el document index i ens el mostra per pantalla.

![Arxius dins del ZIP](imatges/maquetacio/13.png){ height=200px }

Podem veure que *Aules* ha ens dona l'opció a l'arrastrar i soltar de poder pujar-ho com a arxiu:

![Opció d'*Aules*](imatges/maquetacio/14.png){ height=200px }

D'aquesta manera al fer clic sobre l'enllaç i ajustar els paràmetres de mida de finestra podem veure que el document queda fomatat de manera estandarditzada:

![Disseny del document a *Aules*](imatges/maquetacio/15.png){ height=280px }

:::note
De la mateixa manera que hem comentat amb l'arxiu PDF fem ús de plantilles, en aquest cas utilitzem una plantilla anomenada elegant_bootstrap_menu, a la que afegim una plantilla CSS creada per nosaltres per a donar-li un disseny més personalitzat
:::

\awesomebox[violet]{2pt}{\faGithub}{violet}{Es pot consultar el codi de l'aplicació al següent enllaç: https://gitlab.com/alviboi/dam\_aplicacio\_assessor}

# Futures actualitzacions

Aquest projecte ha abordat la fase de desenvolupament del projecte. Caldria crear una estratègia per la fase de desplegament i de proves que es preveu realitzar-se durant el primer trimestre del curs vinent. Seria molt ingenu pensar que tot el projecte està acabat i que no van haver errades, ha sigut llarg i complex.

Altre dels punts a considerar és la depuració del codi i correcció d'errades així com donar-li una altra passada al disseny de l'aplicació. S'ha creat d'una manera molt sòbria centrant-se només en la funcionalitat.

# Conclusions

Cal dir que el projecte ha sigut exasperant a estones, l'he patit i encara li queda molt de camí per recórrer. Probablement la planificació ha sigut un tant optimista sense parar-se a pensat que hi han moltes eines que cal estudiar, revisar i provar... provar molt...

Tot i això, l'aplicació simplifica moltes de les situacions que s'estaven donant al CEFIRE i que calia abordar. En tres mesos aquest projecte ha passat de ser una declaració d'intencions per part d'un únic assessor de la xarxa CEFIRE a que en aquesta mateixa setmana que estic escrivint aquesta memòria rebre una cridada per part del subdirector general de formació del professorat per a rebre el seu recolzament i plantejar la possibilitat de muntar-se en tots els CEFIRES de la Comunitat, aprofitant que es munte l'aplicació de fitxatges ja que tenim funcionant al CEFIRE de València.

En la part tècnica, tal vegada haja sigut massa atrevit per la meua part plantejar una aplicació amb entorn gràfic amb Java, quan pràcticament no hem tingut formació al respecte, al igual que moltes de les llibreries utilitzades. He deprés molt, més que a les mateixes assignatures del ciclen que, tal vegada, es plategen com propedèutiques per al que ve després.

Segurament m'haguera resultat més senzill plantejar el projecte amb **Glade** i Python, però és de sobra conegut que la DGTIC crea les aplicacions pròpies amb Java, per això la decisió s'ha de prendre també des d'una perspectiva professional. Ara crec que ja sé programar un poc més amb Java, però em queda molt per aprendre...

# Bibliografia i referències

(@) [Documentation JavaFx \[ORACLE\]](https://docs.oracle.com/JavaFx/2/)
(@) [OpenCV  Open Source Computer Vision](https://docs.opencv.org/4.x/d6/d00/tutorial_py_root.html)
(@) [Documentation PDFBox \[APACHE\]](https://PDFbox.apache.org/)
(@) [Documentation Pandoc \[John MacFarlane\]](https://pandoc.org)
(@) [Documentation Java \[ORACLE\]](https://docs.oracle.com/en/Java/)
(@) [PySimpleGUI User's Manual \[PySimpleGUI\]](https://pysimplegui.raadthedocs.io/en/latest/)
(@) [Python 3.10.4 documentation\[Python Software Foundation\]](https://docs.python.org/3/)